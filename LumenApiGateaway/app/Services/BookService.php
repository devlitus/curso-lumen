<?php


namespace App\Services;


use App\Traits\ConsumesExternalService;

class BookService
{
    use ConsumesExternalService;

    /**
     * The base uri to be used to consume the book service
     * @var string
     */
    public $baseUri;
    public function __construct()
    {
        $this->baseUri = config('services.book.base_uri');
    }
}
