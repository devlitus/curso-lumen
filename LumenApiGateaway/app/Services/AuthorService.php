<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class AuthorService
{
    use ConsumesExternalService;
    /**
     * The base uri to be used to consume the author service
     * @var string
     */
    public $baseUri;

    public function __construct()
    {
        $this->baseUri = config('services.author.base_uri');
    }

    /**
     * Get the full list of authors from the author service
     * @return string
     */
    public function obtainAuthors()
    {
        return $this->performRequest('GET', '/authors');
    }

    /**
     * Create an instance of author using the authors service
     * @param $data
     * @param int $HTTP_CREATED
     * @return string
     */
    public function createAuthor($data, int $HTTP_CREATED)
    {
        return $this->performRequest('POST', '/authors', $data);
    }

    /**
     * Get a single author from the author service
     * @param $author
     * @return string
     */
    public function obtainAuthor($author)
    {
        return $this->performRequest('GET', "/authors/{$author}");
    }
    public function editAuthor($data, $author)
    {
        return $this->performRequest('PUT', "/authors/{$author}", $data);
    }
}
